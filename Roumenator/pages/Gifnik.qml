import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "../json"
import "../images"
import "./"

Page {

    API {
        id: api
    }

    header: PageHeader {
        id: gifnikheader
        leadingActionBar.actions: [
            Action {
                iconName: "stock_image"
                text: "Rouming"
                onTriggered: {
                  layoutbody.primaryPageSource = rouming
                }
            },
            Action {
                iconSource: "../images/gif.svg"
                text: "Gifník"
                onTriggered: {
                  layoutbody.primaryPageSource = gifnik
                }
            },
            Action {
                iconSource: "../images/jokes.svg"
                text: "Vtipy"
                onTriggered: {
                  layoutbody.primaryPageSource = vtipy
                }
            },
            Action {
                iconName: "stock_message"
                text: "Fórum"
                onTriggered: {
                  layoutbody.primaryPageSource = forum
                }
            },
            Action {
                iconSource: "../images/meat.svg"
                text: "Maso"
                onTriggered: {
                  Prop.backcolor = "#22232A"
                  Prop.haldercolor = "#02030A"
                  layoutbody.primaryPageSource = maso
                }
            }
        ]
        trailingActionBar {
            actions: [
                Action {
                    iconName: "settings"
                    text: "Nastavení"
                    onTriggered: gifniktab.pageStack.addPageToNextColumn(gifniktab, nastaveni)
                }
           ]
        }
        title: "Gifník"
        StyleHints {
            foregroundColor: "white"
            backgroundColor: "#1c1954"
        }
    }

    ListView {
         anchors.fill: parent
         anchors.topMargin: Qt.application.font.pixelSize * 3.5
         id: list1
         width: parent.width
         height: parent.height
         clip: true

         JSONListModel {
             id: jsonModel1
             source: api.gifnik

             query: "$[*]"
         }
         model: jsonModel1.model

         section.property: "name"
         section.criteria: ViewSection.FirstCharacter

         delegate: Rectangle {
             height: Qt.application.font.pixelSize * 7.9
             width: parent.width
             color: "#1c1975"
             MouseArea {
                     anchors.fill: parent
                     onClicked: {
                         gifniktab.pageStack.addPageToNextColumn(gifniktab, gifview)
                         Prop.gifview1 = model.url
                         Prop.nadpisg = model.name
                         Prop.play = true
                         Prop.paus = false
                         Prop.comments = model.object
                         Prop.originallink = model.originalURL
                     }
                 }
             Image {
                 id: pic
                 height: Qt.application.font.pixelSize * 7.9
                 width: Qt.application.font.pixelSize * 7.9
                 source: model.url
             }

             Text {
                 id: nadpis
                 wrapMode: Text.WordWrap
                 maximumLineCount: 1
                 anchors.left: pic.right
                 width: (parent.width) - Qt.application.font.pixelSize * 7.9
                 horizontalAlignment: Text.AlignLeft
                 font.pixelSize: Qt.application.font.pixelSize * 1.5
                 color: "white"
                 text: " " + model.name
             }
             Text {
                 id: popkom
                 wrapMode: Text.WordWrap
                 anchors.left: pic.right
                 anchors.top: nadpis.bottom
                 width: (parent.width) - Qt.application.font.pixelSize * 7.9
                 horizontalAlignment: Text.AlignLeft
                 font.pixelSize: Qt.application.font.pixelSize * 1.3
                 color: "grey"
                 text: " Poslední komentář:"
             }
             Text {
                 id: coment
                 wrapMode: Text.WordWrap
                 maximumLineCount: 3
                 anchors.left: pic.right
                 anchors.top: popkom.bottom
                 width: (parent.width) - Qt.application.font.pixelSize * 7.9
                 horizontalAlignment: Text.AlignLeft
                 font.pixelSize: Qt.application.font.pixelSize * 1.1
                 color: "lightgrey"
                 text: model.lastComment
             }
             Text {
                 id: down
                 anchors.left: pic.right
                 anchors.bottom: pic.bottom
                 width: (parent.width) - Qt.application.font.pixelSize * 7.9
                 horizontalAlignment: Text.AlignRight
                 font.pixelSize: Qt.application.font.pixelSize * 0.9
                 text: "<font color='white'>" + model.rank + " </font>"
             }
             Text {
                 id: datetime
                 anchors.left: pic.right
                 anchors.bottom: pic.bottom
                 width: (parent.width) - Qt.application.font.pixelSize * 7.9
                 horizontalAlignment: Text.AlignLeft
                 font.pixelSize: Qt.application.font.pixelSize * 0.9
                 color: "khaki"
                 text: " " + model.dateInserted
             }
             Rectangle {
                 id: line
                 height: 2
                 width: parent.width
                 color: "lightgrey"
             }
     }
    }
}
