import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "../json"
import "../images"
import "./"

Page {

    API {
        id: api
    }

    header: PageHeader {
        id: forumtabheader
        leadingActionBar.actions: [
            Action {
                iconName: "stock_image"
                text: "Rouming"
                onTriggered: {
                  layoutbody.primaryPageSource = rouming
                }
            },
            Action {
                iconSource: "../images/gif.svg"
                text: "Gifník"
                onTriggered: {
                  layoutbody.primaryPageSource = gifnik
                }
            },
            Action {
                iconSource: "../images/jokes.svg"
                text: "Vtipy"
                onTriggered: {
                  layoutbody.primaryPageSource = vtipy
                }
            },
            Action {
                iconName: "stock_message"
                text: "Fórum"
                onTriggered: {
                  layoutbody.primaryPageSource = forum
                }
            },
            Action {
                iconSource: "../images/meat.svg"
                text: "Maso"
                onTriggered: {
                  Prop.backcolor = "#22232A"
                  Prop.haldercolor = "#02030A"
                  layoutbody.primaryPageSource = maso
                }
            }
        ]
        trailingActionBar {
            actions: [
                Action {
                    iconName: "settings"
                    text: "Nastavení"
                    onTriggered: {
                      forumtab.pageStack.addPageToNextColumn(forumtab, nastaveni)
                    }
                }
           ]
        }
        title: "Fórum"
        StyleHints {
            foregroundColor: "white"
            backgroundColor: "#1c1954"
        }
    }

    ListView {
      anchors.top: forumtabheader.bottom
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: parent.right
      id: list1
      width: parent.width
      height: parent.height
      clip: true

      JSONListModel {
          id: jsonModel1
          source: api.vlaknafora

          query: "$[*]"
      }
      model: jsonModel1.model

      section.property: "name"
      section.criteria: ViewSection.FirstCharacter

      delegate: Rectangle {
          height: (mezera.height) + (nadpis.height) + (down.height)
          width: parent.width
          color: "#1c1975"
          MouseArea {
                  anchors.fill: parent
                  onClicked: {
                      forumtab.pageStack.addPageToNextColumn(forumtab, forumview)
                      Prop.forumthread = model.thread
                      Prop.nadpisk = model.description
                  }
              }
          Text {
              id: nadpis
              wrapMode: Text.WordWrap
              maximumLineCount: 2
              width: parent.width
              horizontalAlignment: Text.AlignLeft
              font.pixelSize: Qt.application.font.pixelSize * 1.5
              color: "yellow"
              text: model.description
          }
          Text {
              id: mezera
              wrapMode: Text.WordWrap
              anchors.top: nadpis.bottom
              width: parent.width
              horizontalAlignment: Text.AlignLeft
              font.pixelSize: Qt.application.font.pixelSize * 0.5
              color: "lightgrey"
              text: "\n"
          }
          Text {
              id: down
              anchors.top: mezera.bottom
              width: parent.width
              horizontalAlignment: Text.AlignRight
              font.pixelSize: Qt.application.font.pixelSize * 1.3
              text: "<font color='grey'>" + model.posts + " příspěvků </font>"
          }
          Rectangle {
              id: line
              height: 2
              width: parent.width
              color: "lightgrey"
          }
  }
    }
}
