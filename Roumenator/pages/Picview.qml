import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "./"


Page {
  id: picviewpage
  header: PageHeader {
      id: picheader
      title: Prop.nadpis
      StyleHints {
          foregroundColor: "white"
          backgroundColor: "#1c1954"
        }
        trailingActionBar {
          actions: [
            Action {
              iconName: "stock_note"
              text: "Komentáře"
              onTriggered: picview.pageStack.addPageToNextColumn(picview, komentare)
            },
            Action {
                iconName: "edit-copy"
                text: "Kopírovat odkaz"
                onTriggered: Propertys.toclip(Prop.originallink)
            }
          ]
          numberOfSlots: 1
        }
      }


    Flickable {
        id: flickimage
        focus: true
        clip: true
        anchors.top: picheader.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        contentWidth: parent.width
        contentHeight: parent.height

        Image {
            id: imageview1
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: Prop.picview1
            onSourceChanged: {
                flickimage.resizeContent(picviewpage.width, picviewpage.height, imageview1.Center)
                flickimage.returnToBounds()
                Prop.sized = false
            }
        }
        MouseArea {
                anchors.fill: parent
                onDoubleClicked: {
                    if(Prop.sized == false) {
                        flickimage.resizeContent(imageview1.sourceSize.width * 1.5, imageview1.sourceSize.height * 1.5, imageview1.Center)
                        Prop.sized = true
                    }
                    else {
                        flickimage.resizeContent(picviewpage.width, picviewpage.height, imageview1.Center)
                        flickimage.returnToBounds()
                        Prop.sized = false
                    }
                    }
        }

    }



}
