import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "../images"


Page {

    header: PageHeader {
        title: "O Aplikaci"
        StyleHints {
            foregroundColor: "white"
            backgroundColor: Prop.haldercolor
          }
        }

    ListView {
        anchors.fill: parent
        width: parent.width
        height: parent.height
        clip: true
        model: 1
        delegate: Item {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: Qt.application.font.pixelSize * 50.5
            Rectangle {
                id: logo
                y: Qt.application.font.pixelSize * 5
                anchors.horizontalCenter: parent.horizontalCenter
                height: Qt.application.font.pixelSize * 9.3
                width: Qt.application.font.pixelSize * 9.3
                color: "#484848"
                Image {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: Qt.application.font.pixelSize * 9
                    width: Qt.application.font.pixelSize * 9
                    source: "../images/logo.png"
                }
            }
            Text {
                id: nazev
                anchors.top: logo.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 1
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Qt.application.font.pixelSize * 1.8
                color: "white"
                text: "Roumenator"
            }
            Text {
                id: verze
                anchors.top: nazev.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 0.3
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Qt.application.font.pixelSize * 1.2
                color: "grey"
                text: "Verze: 0.1.9.1"
            }
            Rectangle {
                id: line
                height: 2
                anchors.top: verze.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 6
                width: parent.width
                color: "grey"
            }
            Rectangle {
                id: zdroj
                color: Prop.backcolor
                height: Qt.application.font.pixelSize * 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: line.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 1
                Text {
                    anchors.left: parent.left
                    anchors.leftMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "Získat zdrojový kód"
                }
                Text {
                    anchors.right: parent.right
                    anchors.rightMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "<font>\u2192</font>"
                }
                MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            Qt.openUrlExternally("https://gitlab.com/x_Dub_CZ/roumenator");
                        }
                }
            }
            Rectangle {
                id: line2
                height: 2
                anchors.top: zdroj.bottom
                width: parent.width
                color: "grey"
            }
            Rectangle {
                id: bugs
                color: Prop.backcolor
                height: Qt.application.font.pixelSize * 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: line2.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 1
                Text {
                    anchors.left: parent.left
                    anchors.leftMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "Nahlásit chyby"
                }
                Text {
                    anchors.right: parent.right
                    anchors.rightMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "<font>\u2192</font>"
                }
                MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            Qt.openUrlExternally("https://gitlab.com/x_Dub_CZ/roumenator/issues");
                        }
                }
            }
            Rectangle {
                id: line3
                height: 2
                anchors.top: bugs.bottom
                width: parent.width
                color: "grey"
            }
            Rectangle {
                id: templates
                color: Prop.backcolor
                height: Qt.application.font.pixelSize * 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: line3.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 1
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "yellow"
                    text: "Použité projekty:"
                }
            }
            Rectangle {
                id: line4
                height: 2
                anchors.top: templates.bottom
                width: parent.width
                color: "grey"
            }
            Rectangle {
                id: shaproject
                color: Prop.backcolor
                height: Qt.application.font.pixelSize * 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: line4.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 1
                Text {
                    anchors.left: parent.left
                    anchors.leftMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "js-sha256"
                }
                Text {
                    anchors.right: parent.right
                    anchors.rightMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "<font>\u2192</font>"
                }
                MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            Qt.openUrlExternally("https://github.com/emn178/js-sha256");
                        }
                }
            }
            Rectangle {
                id: line5
                height: 2
                anchors.top: shaproject.bottom
                width: parent.width
                color: "grey"
            }
            Rectangle {
                id: jsonlistproject
                color: Prop.backcolor
                height: Qt.application.font.pixelSize * 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: line5.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 1
                Text {
                    anchors.left: parent.left
                    anchors.leftMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "JSONListModel"
                }
                Text {
                    anchors.right: parent.right
                    anchors.rightMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "<font>\u2192</font>"
                }
                MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            Qt.openUrlExternally("https://github.com/kromain/qml-utils");
                        }
                }
            }
            Rectangle {
                id: line6
                height: 2
                anchors.top: jsonlistproject.bottom
                width: parent.width
                color: "grey"
            }

            Rectangle {
                id: specthanks
                color: Prop.backcolor
                height: Qt.application.font.pixelSize * 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: line6.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 1
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "yellow"
                    text: "Speciální poděkování:"
                }
            }
            Rectangle {
                id: line8
                height: 2
                anchors.top: specthanks.bottom
                width: parent.width
                color: "grey"
            }
            Rectangle {
                id: kozub
                color: Prop.backcolor
                height: Qt.application.font.pixelSize * 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: line8.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 1
                Text {
                    anchors.left: parent.left
                    anchors.leftMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "Odborná pomoc:"
                }
                Text {
                    anchors.right: parent.right
                    anchors.rightMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "<font>Martin Kozub</font>"
                }
            }
            Rectangle {
                id: line9
                height: 2
                anchors.top: kozub.bottom
                width: parent.width
                color: "grey"
            }
            Rectangle {
                id: korecky
                color: Prop.backcolor
                height: Qt.application.font.pixelSize * 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: line9.bottom
                anchors.topMargin: Qt.application.font.pixelSize * 1
                Text {
                    anchors.left: parent.left
                    anchors.leftMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "Neúnavné testování:"
                }
                Text {
                    anchors.right: parent.right
                    anchors.rightMargin: Qt.application.font.pixelSize * 1
                    font.pixelSize: Qt.application.font.pixelSize * 1.3
                    color: "white"
                    text: "<font>Milan Korecký</font>"
                }
            }
            Rectangle {
                id: line10
                height: 2
                anchors.top: korecky.bottom
                width: parent.width
                color: "grey"
            }
        }


    }

}
