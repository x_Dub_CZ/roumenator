import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "../json"
import "../images"
import "./"

Page {

    property int pagenumber: 0
    property int vybranakategorie: 0

    API {
        id: api
    }

    header: PageHeader {
        id: vtipheader
        leadingActionBar.actions: [
            Action {
                iconName: "stock_image"
                text: "Rouming"
                onTriggered: {
                  layoutbody.primaryPageSource = rouming
                }
            },
            Action {
                iconSource: "../images/gif.svg"
                text: "Gifník"
                onTriggered: {
                  layoutbody.primaryPageSource = gifnik
                }
            },
            Action {
                iconSource: "../images/jokes.svg"
                text: "Vtipy"
                onTriggered: {
                  layoutbody.primaryPageSource = vtipy
                }
            },
            Action {
                iconName: "stock_message"
                text: "Fórum"
                onTriggered: {
                  layoutbody.primaryPageSource = forum
                }
            },
            Action {
                iconSource: "../images/meat.svg"
                text: "Maso"
                onTriggered: {
                  Prop.backcolor = "#22232A"
                  Prop.haldercolor = "#02030A"
                  layoutbody.primaryPageSource = maso
                }
            }
        ]
        trailingActionBar {
            actions: [
                Action {
                    iconName: "settings"
                    text: "Nastavení"
                    onTriggered: vtipytab.pageStack.addPageToNextColumn(vtipytab, nastaveni)
                }
           ]
        }
        title: "Vtipy"
        StyleHints {
            foregroundColor: "white"
            backgroundColor: "#1c1954"
        }
    }
    Text {
        id:kat1
        anchors.top: vtipheader.bottom
        font.pixelSize: Qt.application.font.pixelSize * 1.9
        color: "yellow"
        text: "Kategorie: "
    }
    ComboBox {
        id: kategorie
        anchors.top: vtipheader.bottom
        anchors.left: kat1.right
        anchors.right: parent.right
        height: Qt.application.font.pixelSize * 2.3
        font.pixelSize: Qt.application.font.pixelSize * 1.6
        popup.font.pixelSize: Qt.application.font.pixelSize * 1.6
        //model: ["Vše", "Alkohol", "Blondýny", "Cikáni", "Děti", "Doktoři", "Důchodci", "Gayové", "Kameňáky", "Lechtivé", "Lesbičky", "Manželství", "Maso", "Muži", "Národnosti", "Počítače", "Policajti", "Politika", "Práce", "Skoti", "Škola", "Zamyšlení", "Zvířata", "Ženy", "Židi", "Ostatní"]
        model: ["Vše", "Ženy", "Blondýny", "Lesbičky", "Muži", "Gayové", "Policajti", "Důchodci", "Manželství", "Lechtivé", "Děti", "Doktoři", "Židi", "Cikáni", "Skoti", "Počítače", "Maso", "Kameňáky", "Zvířata", "Alkohol", "Ostatní", "Škola", "Práce", "Politika", "Zamyšlení", "Národnosti"]
        onCurrentIndexChanged: {
            pagenumber = 0
            vybranakategorie = kategorie.currentIndex
        }
    }
    Rectangle {
        id: linekat
        anchors.top: kategorie.bottom
        height: Qt.application.font.pixelSize * 0.2
        width: parent.width
        color: "#1c1975"
    }

    UbuntuListView {
        anchors.top: linekat.bottom
        //anchors.bottom: lastline.top - Příprava na zarovnání k dolnímu baru
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        id: list1
        width: parent.width
        height: parent.height
        clip: true

        ButtonGroup {
            buttons: list1.contentItem.children
        }

        JSONListModel {
            id: jsonModel1
            source: api.vtipy + "&type=" + vybranakategorie + "&page=" + pagenumber + "&deleted=" + Prop.smazanevtipy
            query: "$[*]"
        }
        model: jsonModel1.model

        section.property: "name"
        section.criteria: ViewSection.FirstCharacter

        delegate: ListItem {
            highlightColor: "#1c1975"
            color: "#1c1975"
            height: (text1.height) + (nadpis.height) + (down.height)
            width: parent.width

                Item {
                    id: items1
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    Text {
                        id: nadpis
                        wrapMode: Text.WordWrap
                        maximumLineCount: 1
                        width: parent.width
                        horizontalAlignment: Text.AlignLeft
                        font.pixelSize: Qt.application.font.pixelSize * 1.5
                        color: "white"
                        text: model.name
                    }
                    Text {
                        id: text1
                        wrapMode: Text.WordWrap
                        anchors.top: nadpis.bottom
                        width: parent.width
                        horizontalAlignment: Text.AlignLeft
                        font.pixelSize: Qt.application.font.pixelSize * 1.3
                        color: "lightgrey"
                        text: model.text
                    }
                    Text {
                        id: down
                        anchors.top: text1.bottom
                        width: parent.width
                        horizontalAlignment: Text.AlignRight
                        font.pixelSize: Qt.application.font.pixelSize * 1.2
                        text: "<font color='white'>" + model.rank + " </font>"
                    }
                    Text {
                        id: datetime
                        anchors.top: text1.bottom
                        width: parent.width
                        horizontalAlignment: Text.AlignLeft
                        font.pixelSize: Qt.application.font.pixelSize * 1.2
                        color: "khaki"
                        text: model.typeName
                    }
                }

/*             checked: swipe.complete
            onCheckedChanged: if (!checked) swipe.close()
            swipe.right: Label {
                id: toclipboard
                verticalAlignment: Label.AlignVCenter
              //  padding: Qt.application.font.pixelSize * 2
                height: parent.height
                anchors.right: parent.right

                MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            swipe.close()
                            Propertys.toclip(model.text)
                        }
                    }

               background: Rectangle {
                    id: toclipboardback
                    color: toclipboard.SwipeDelegate.pressed ? Qt.darker("navy", 1.1) : "navy"
                    Image {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: Qt.application.font.pixelSize * 1.6
                        width: Qt.application.font.pixelSize * 1.6
                        source: "./images/copy-icon.png"
                    }
                }
            }*/

            trailingActions: ListItemActions {
                actions: [
                    Action {
                        iconName: "edit-copy"
                        onTriggered: {
                          Propertys.toclip(model.text)
                        }
                    }
                ]
            }
            Rectangle {
                id: line
                 height: 2
                 width: parent.width
                 color: "lightgrey"
            }
        }




    }
    //Pokus o stránkování vtipů - zatím moc nedořešených problémů
    /*Rectangle {
        id: lastline
        anchors.bottom: strana.top
        height: 2
        width: parent.width
        color: "#1c1954"
    }
    ToolButton {
        id: strana
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        text: "<font color='white'>\u2190</font>"
        font.pixelSize: Qt.application.font.pixelSize * 1.5
        onClicked: {
            if(pagenumber > 0)
            {
                pagenumber--
            }
        }
    }
    Text {
        text: "<font color='white'>Strana " + (pagenumber + 1) + " z 1000</font>"
        anchors.top: list1.bottom
        anchors.bottom: parent.bottom
        anchors.right: strana1.left
        anchors.left: strana.right
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
        font.pixelSize: Qt.application.font.pixelSize * 1.1
    }
    ToolButton {
        id: strana1
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        text: "<font color='white'>\u2192</font>"
        font.pixelSize: Qt.application.font.pixelSize * 1.5
        onClicked: {
            pagenumber++
        }
    }*/
}
