import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "../images"
import "../json"
import "../sha256/sha256.js" as JSSHA256
import "./"



Page {

    property int pagenumber: 0

    header: PageHeader {

        title: "Login"
        StyleHints {
            foregroundColor: "white"
            backgroundColor: Prop.haldercolor
        }
    }

    API {
        id: api
    }

    /*JSONListModel {
        id: jsonModel1
        //source: api.loginacces + "&nick=" + user.text.toLowerCase() + "&password=" + JSSHA256.root().sha256(user.text.toLowerCase() + pass.text)
        query: "$[*]"
        onSourceChanged: {
          Prop.status = jsonModel1.model.get(0).status
          //Prop.status = jsonModel1.source
        }

    }*/

    Rectangle {
        id: odstup
        anchors.top: parent.top
        height: Qt.application.font.pixelSize * 4
        width: parent.width
        color: Prop.backcolor
    }


    Text {
        id:userpopis
        x: Qt.application.font.pixelSize * 0.4
        font.pixelSize: Qt.application.font.pixelSize * 1.3
        anchors.top: odstup.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        color: "white"
        text: "Uživatelské jméno: "
    }

    TextField {
        id: user
        width: Qt.application.font.pixelSize * 15
        height: Qt.application.font.pixelSize * 2
        anchors.top: odstup.bottom
        anchors.topMargin: Qt.application.font.pixelSize * (0.8 + 0.8 + 1.3)
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id:passpopis
        x: Qt.application.font.pixelSize * 0.4
        font.pixelSize: Qt.application.font.pixelSize * 1.3
        anchors.top: user.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        color: "white"
        text: "Heslo: "
    }

    TextField {
        id: pass
        echoMode: TextInput.Password
        width: Qt.application.font.pixelSize * 15
        height: Qt.application.font.pixelSize * 2
        anchors.top: user.bottom
        anchors.topMargin: Qt.application.font.pixelSize * (0.8 + 0.8 + 1.3)
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Rectangle {
        id: prihlaseni
        width: Qt.application.font.pixelSize * 15
        height: Qt.application.font.pixelSize * 2
        radius: 10
        border.color: "lightgrey"
        border.width: Qt.application.font.pixelSize * 0.1
        color: prihlaseniclick.pressed ? "darkgreen" : "green"
        anchors.top: pass.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.8
        anchors.horizontalCenter: parent.horizontalCenter
        Text {
            font.pixelSize: Qt.application.font.pixelSize * 1.3
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            color: "white"
            text: "Přihlásit se"
        }
        MouseArea {
            id: prihlaseniclick
            anchors.fill: parent
            onClicked: {
                //jsonModel1.source = api.loginacces + "&nick=" + user.text.toLowerCase() + "&password=" + JSSHA256.root().sha256(user.text.toLowerCase() + pass.text)
                popup.open()
            }
        }
    }


    Popup {
      id: popup
      leftMargin: (parent.width / 2) - (popup.width / 2)
      topMargin: (parent.height / 2) - (popup.height / 2)
      width: Qt.application.font.pixelSize * 20
      height: Qt.application.font.pixelSize * 10
      background: Rectangle {
                  border.color: Prop.haldercolor
                  border.width: Qt.application.font.pixelSize * 0.3
                  color: Prop.backcolor
              }
      modal: true
      focus: true
      Text {
          id:textpopup
          font.pixelSize: Qt.application.font.pixelSize * 1.3
          anchors.top: parent.top
          anchors.topMargin: Qt.application.font.pixelSize * 2
          anchors.horizontalCenter: parent.horizontalCenter
          color: "white"
          text: "Zatím v přípravě. :-("
      }
      Rectangle {
          id: okbutton
          width: Qt.application.font.pixelSize * 15
          height: Qt.application.font.pixelSize * 2
          radius: 10
          border.color: "lightgrey"
          border.width: Qt.application.font.pixelSize * 0.1
          color: okbuttonclick.pressed ? "darkgreen" : "green"
          anchors.top: textpopup.bottom
          anchors.topMargin: Qt.application.font.pixelSize * 0.8
          anchors.horizontalCenter: parent.horizontalCenter
          Text {
              font.pixelSize: Qt.application.font.pixelSize * 1.3
              anchors.verticalCenter: parent.verticalCenter
              anchors.horizontalCenter: parent.horizontalCenter
              color: "white"
              text: "OK"
          }
          MouseArea {
              id: okbuttonclick
              anchors.fill: parent
              onClicked: {
                  popup.close()
                  login.pageStack.removePages(login)
              }
          }
    }
  }


}
