import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "../json"
import "./"


Page {

    API {
        id: api
    }


    header: PageHeader {
        id: forumheader
        title: Prop.nadpisk
        StyleHints {
            foregroundColor: "white"
            backgroundColor: "#1c1954"
          }
        }

    ListView {
      anchors.top: forumheader.bottom
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: parent.right
      id: list1
      width: parent.width
      height: parent.height
      clip: true


      JSONListModel {
          id: jsonModel1
          source: api.forum + "&thread=" + Prop.forumthread

          query: "$[*]"
      }
      model: jsonModel1.model

      section.property: "name"
      section.criteria: ViewSection.FirstCharacter

      delegate: Rectangle {
          height: (nick.height) + (text1.height) + (datetime.height)
          width: parent.width
          color: "#1c1975"
          Text {
              id: nick
              wrapMode: Text.WordWrap
              maximumLineCount: 1
              width: parent.width
              horizontalAlignment: Text.AlignLeft
              font.pixelSize: Qt.application.font.pixelSize * 1.4
              color: "white"
              text: model.nick
          }
          Text {
              id: text1
              wrapMode: Text.WordWrap
              anchors.top: nick.bottom
              width: parent.width
              horizontalAlignment: Text.AlignLeft
              font.pixelSize: Qt.application.font.pixelSize * 1.2
              color: "khaki"
              text: model.message
          }
          Text {
              id: datetime
              anchors.top: text1.bottom
              width: parent.width
              horizontalAlignment: Text.AlignLeft
              font.pixelSize: Qt.application.font.pixelSize * 1.3
              color: "grey"
              text: model.time
          }
          Rectangle {
              id: line
              height: 2
              width: parent.width
              color: "lightgrey"
          }
  }
    }



}
