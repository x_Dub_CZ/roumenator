import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "./"
import "../images"


Page {

    property int pagenumber: 0

    header: PageHeader {
        trailingActionBar {
          actions: [
            Action {
              iconName: "info"
              text: "Info"
              onTriggered: nastaveni.pageStack.addPageToNextColumn(nastaveni, info)
            }
          ]
        }
        title: "Nastavení"
        StyleHints {
            foregroundColor: "white"
            backgroundColor: Prop.haldercolor
        }
    }

    Rectangle {
        id: odstup
        anchors.top: parent.top
        height: Qt.application.font.pixelSize * 4
        width: parent.width
        color: Prop.backcolor
    }


    Text {
        id:popis1
        x: Qt.application.font.pixelSize * 0.4
        font.pixelSize: Qt.application.font.pixelSize * 1.3
        anchors.top: odstup.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.8
        anchors.verticalCenter: pages.verticalCenter
        color: "white"
        text: "Spustit aplikaci na stránce: "
    }
    ComboBox {
        id: pages
        anchors.left: popis1.right
        anchors.right: parent.right
        anchors.top: odstup.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.6
        height: Qt.application.font.pixelSize * 1.9
        width: Qt.application.font.pixelSize * 7.6
        font.pixelSize: Qt.application.font.pixelSize * 1.4
        popup.font.pixelSize: Qt.application.font.pixelSize * 1.2
        model: ["Rouming", "Gifník", "Vtipy", "Fórum"]
        Component.onCompleted: {
        currentIndex = Prop.vyberstartpage
        }
        onActivated: {
            if (pages.currentIndex == 0) {
                Prop.startpage = "Roumingtab.qml"
                Prop.vyberstartpage = 0
            }
            if (pages.currentIndex == 1) {
                Prop.startpage = "Gifnik.qml"
                Prop.vyberstartpage = 1
            }
            if (pages.currentIndex == 2) {
                Prop.startpage = "Vtipy.qml"
                Prop.vyberstartpage = 2
            }
            if (pages.currentIndex == 3) {
                Prop.startpage = "Forum.qml"
                Prop.vyberstartpage = 3
            }
        }

    }
    Rectangle {
        id: line
        height: 2
        anchors.top: popis1.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.8
        width: parent.width
        color: "grey"
    }
    CheckBox {
        id: smazanevtipy
        checked: Prop.smazanevtipychacket
        anchors.top: line.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.8
//        font.pixelSize: Qt.application.font.pixelSize * 1.3
        text: qsTr("<font color='white'>Zobrazit smazané vtipy</font>")
        onClicked: {
            if(smazanevtipy.checked) {
                Prop.smazanevtipy = 1
                Prop.smazanevtipychacket = true
            }
            else {
                Prop.smazanevtipy = 0
                Prop.smazanevtipychacket = false
            }
        }
        }
    Rectangle {
        id: line2
        height: 2
        anchors.top: smazanevtipy.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.8
        width: parent.width
        color: "grey"
    }
    Text {
        id: prihlasenistatus
        x: Qt.application.font.pixelSize * 0.4
        font.pixelSize: Qt.application.font.pixelSize * 1.3
        anchors.top: line2.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.8
        anchors.verticalCenter: pages.verticalCenter
        anchors.horizontalCenter: odstup.horizontalCenter
        color: "red"
        text: "Nepřihlášen"
    }
    Rectangle {
        id: prihlaseni
        width: Qt.application.font.pixelSize * 15
        height: Qt.application.font.pixelSize * 2
        radius: 10
        border.color: "lightgrey"
        border.width: Qt.application.font.pixelSize * 0.1
        color: prihlaseniclick.pressed ? "darkgreen" : "green"
        anchors.top: line2.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 2.8
        anchors.horizontalCenter: odstup.horizontalCenter
        Text {
            font.pixelSize: Qt.application.font.pixelSize * 1.3
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            color: "white"
            text: "Přihlásit se"
        }
        MouseArea {
            id: prihlaseniclick
            anchors.fill: parent
            onClicked: {
                nastaveni.pageStack.addPageToNextColumn(nastaveni, login)
            }
        }
    }
    Rectangle {
        id: line3
        height: 2
        anchors.top: prihlaseni.bottom
        anchors.topMargin: Qt.application.font.pixelSize * 0.8
        width: parent.width
        color: "grey"
    }

}
