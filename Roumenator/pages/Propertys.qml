pragma Singleton
import QtQuick 2.0
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3

Item {
    property url picview1
    property url gifview1
    property int tab: 0
    property string pageb2: "Picview.qml"
    property string nadpis: "Prohlížeč obrázků"
    property string nadpisg: "Prohlížeč gifů"
    property string nadpisk: ""
    property string backcolor: "#1c1975"
    property string haldercolor: "#1c1954"
    property bool play: true
    property bool paus: false
    property string forumthread
    property string comments
    property bool picmenubut: true
    property bool setmenubut: false
    property bool aboutclosebut: false
    property bool komclosebut: false
    property bool backbut: true
    property bool fakebut: false
    property string startpage: settings.setstartpage
    property int smazanevtipy: settings.setsmazanevtipy
    property bool smazanevtipychacket: settings.setsmazanevtipychacked
    property int vyberstartpage: settings.setvyberstartpage
    property string originallink: ""
    property bool sized: false
    property string status: ""

    Settings {
        id: settings
        property string setstartpage: "Roumingtab.qml"
        property int setsmazanevtipy: 0
        property bool setsmazanevtipychacked: false
        property int setvyberstartpage: 0
    }
    Component.onDestruction: {
        settings.setstartpage = startpage
        settings.setsmazanevtipy = smazanevtipy
        settings.setsmazanevtipychacked = smazanevtipychacket
        settings.setvyberstartpage = vyberstartpage
        }
    function toclip(toclipdata) {
        Clipboard.push(toclipdata)
    }

}
