import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "../json"
import "../images"
import "./"

Page {
  id: masogiftab
  header: PageHeader {
      id: masogiftabheader
      leadingActionBar.actions: [
          Action {
              iconSource: "../images/meat.svg"
              text: "Maso"
              onTriggered: {
              layoutbody.primaryPageSource = maso
            }
          },
          Action {
              iconSource: "../images/meat-gif.svg"
              text: "Maso Gifník"
              onTriggered: {
                layoutbody.primaryPageSource = masogif
              }
          },
          Action {
              iconName: "stock_image"
              text: "Rouming"
              onTriggered: {
                Prop.backcolor = "#1c1975"
                Prop.haldercolor = "#1c1954"
                layoutbody.primaryPageSource = rouming
              }
          }
      ]
      trailingActionBar {
          actions: [
              Action {
                  iconName: "settings"
                  text: "Nastavení"
                  onTriggered: masogiftab.pageStack.addPageToNextColumn(masogiftab, nastaveni)
              }
         ]
      }
      title: "Roumenovo Maso - Gifník"
      StyleHints {
          foregroundColor: "white"
          backgroundColor: "#02030A"
      }
  }
  Column {
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.verticalCenter: parent.verticalCenter
      Label {
          text: "Tato sekce se připravuje. 😉"
      }
  }
}
