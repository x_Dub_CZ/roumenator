import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQml 2.2
import Ubuntu.Components 1.3
import "./"


Page {


  id: picviewpage
  header: PageHeader {
      id: gifheader
      title: Prop.nadpisg
      StyleHints {
          foregroundColor: "white"
          backgroundColor: "#1c1954"
        }
        trailingActionBar {
          actions: [
            Action {
              iconName: "stock_note"
              text: "Komentáře"
              onTriggered: gifview.pageStack.addPageToNextColumn(gifview, komentare)
            },
            Action {
                iconName: "edit-copy"
                text: "Kopírovat odkaz"
                onTriggered: Propertys.toclip(Prop.originallink)
            }
          ]
          numberOfSlots: 1
        }
      }




    AnimatedImage {
        id: imageview1
        anchors.top: gifheader.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        fillMode: Image.PreserveAspectFit
        source: Prop.gifview1
        playing: Prop.play
        paused: Prop.paus
    }



}
