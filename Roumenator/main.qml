/*
 * Copyright (C) 2022  Vojtěch Dubský
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * roumenator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "pages"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'roumenator.xdub'
    automaticOrientation: true
    backgroundColor: Prop.backcolor

    function tabselect() {
      if (Prop.startpage == "Roumingtab.qml"){
        layoutbody.primaryPageSource = rouming
      }
      else
      if (Prop.startpage == "Gifnik.qml"){
        layoutbody.primaryPageSource = gifnik
      }
      else
      if (Prop.startpage == "Vtipy.qml"){
        layoutbody.primaryPageSource = vtipy
      }
      else
      if (Prop.startpage == "Forum.qml"){
        layoutbody.primaryPageSource = forum
      }
      else
      {
        layoutbody.primaryPageSource = rouming
      }
    }

    width: units.gu(100)
    height: units.gu(60)
    AdaptivePageLayout {
        id: layoutbody
        anchors.fill: parent
        primaryPageSource: tabselect()
        layouts: PageColumnsLayout {
            when: width > units.gu(80)
            // column #0
            PageColumn {
                minimumWidth: units.gu(30)
                maximumWidth: units.gu(60)
                preferredWidth: units.gu(40)
            }
            // column #1
            PageColumn {
            fillWidth: true
            }
        }


        Component {
          id: rouming
              Roumingtab {
                id: roumingtab
              }
        }
        Component {
          id: gifnik
              Gifnik {
                id: gifniktab
              }
        }
        Component {
          id: vtipy
              Vtipy {
                id: vtipytab
              }
        }
        Component {
          id: forum
              Forum {
                id: forumtab
              }
        }
        Component {
          id: maso
              Maso {
                id: masotab
              }
        }
        Component {
          id: masogif
              Masogif {
                id: masogiftab
              }
        }

            Settings {
              id: nastaveni
            }
            Login {
              id: login
            }
            About {
              id: info
            }
            Picview {
              id: picview
            }
            Gifview {
              id: gifview
            }
            Forumview {
              id: forumview
            }
            Komentare {
              id: komentare
            }
            Page {
                id: page3
                header: PageHeader {
                    title: "Page #3"
                    StyleHints {
                        foregroundColor: "white"
                        backgroundColor: "#1c1954"
                    }
            }
        }
    }
}
