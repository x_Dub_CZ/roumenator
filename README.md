# Roumenator

Czech app for browsing rouming.cz content
_________________________________________

Česká aplikace pro prohlížení obsahu rouming.cz
<div>
<a href="https://open-store.io/app/roumenator.xdub"><img src="https://open-store.io/badges/cs.png" alt="OpenStore" /></a>
</div>

## License

Copyleft 🄯  2019-2020  xDub

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
